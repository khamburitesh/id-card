function Form() {
  const handleSubmit = (e: { preventDefault: () => void }) => {
    e.preventDefault();
    // Handle form submission here
    console.log("Form submitted!");
  };

  return (
    <div className="p-10">
      <div className="card rounded-2xl shadow-2xl bg-white">
        <form onSubmit={handleSubmit}>
          <div className="grid lg:grid-cols-3 sm:grid-cols-2 gap-4">
          <div className="mb-4">
              <label
                htmlFor="file"
                className="block text-lg font-medium leading-6 text-gray-900"
              >
                Upload File
              </label>
              <input
                type="file"
                id="file"
                name="file"
                accept=".jpg, .jpeg, .png, .gif"
                className="block border rounded-md py-[5px] ps-1 pe-3 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-5 border-gray-300 mt-2 w-full"
              />
            </div>
            <div className="mb-4">
              <label
                htmlFor="username"
                className="block text-lg font-medium leading-6 text-gray-900"
              >
                Name
              </label>
              <div className="mt-2">
                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600">
                  <input
                    type="text"
                    name="Name"
                    id="Name"
                    autoComplete="Name"
                    className="block flex-1 border-0 bg-transparent py-1.5 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 pl-3 w-full"
                    placeholder="Hari Sharan Shrestha"
                  />
                </div>
              </div>
            </div>

            <div className="mb-4">
              <label
                htmlFor="username"
                className="block text-lg font-medium leading-6 text-gray-900"
              >
                Designation
              </label>
              <div className="mt-2">
                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600">
                  <input
                    type="text"
                    name="Name"
                    id="Name"
                    autoComplete="Name"
                    className="block flex-1 border-0 bg-transparent py-1.5 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 pl-3"
                    placeholder="Senior Frontend Developer"
                  />
                </div>
              </div>
            </div>
            <div className="mb-4">
              <label
                htmlFor="username"
                className="block text-lg font-medium leading-6 text-gray-900"
              >
                Employee Number
              </label>
              <div className="mt-2">
                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600">
                  <span className="flex select-none items-center pl-3 text-gray-500 sm:text-sm">
                    Employee Number:
                  </span>
                  <input
                    type="text"
                    name="Name"
                    id="Name"
                    autoComplete="Name"
                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                    placeholder="223"
                  />
                </div>
              </div>
            </div>
            
          </div>
          <button
            type="submit"
            className="bg-white text-purple-800 border-purple-400 py-2 rounded-md"
          >
            Save
          </button>
        </form>
      </div>
    </div>
  );
}

export default Form;
