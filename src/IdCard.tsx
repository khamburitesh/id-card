import { useState } from "react";
import "./App.css";
import logo from "./assets/OneWeb_Logo.png";
import cardBg from "./assets/bg-card-2-bg.png";
import bg from "./assets/bg-card-2.png";
import portrait from "./assets/portrait.jpg";

// import './index.css'

function idCard() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [isFlipped, setIsFlipped] = useState(false);

  const flipCard = () => {
    setIsFlipped(!isFlipped);
  };
  return (
    <>
      <div className="p-20 h-full">
        <div className="bg-white rounded-2xl shadow-2xl h-full">
          <div className="flex gap-10 p-10 items-center justify-center h-full w-full">
          <div
              className={`flip-card ${isFlipped ? "flipped" : ""}`}
              onClick={flipCard}
            >
              <div className="flip-card-inner">
                <div className="flip-card-front">
                  <div className="h-[550px] w-[380px] relative rounded-2xl overflow-hidden shadow-2xl">
                    <img src={bg} className="object-cover h-full w-full" alt="" />
                    <div className="absolute top-0 bottom-0 left-0 right-0">
                      <div className="flex justify-center h-full w-full">
                        <div className="flex flex-col items-center pb-3">
                          <img
                            src={portrait}
                            className="h-40 w-40 rounded-full mt-24"
                            alt=""
                          />
                          <div className="grow text-center">
                            <h1 className="text-[24px] mt-16 text-gray-800 font-semibold">
                              Hari Sharan Shrestha
                            </h1>
                            <h1 className="text-[20px] my-3 text-purple-700 font-semibold">
                              Senior Frontend
                            </h1>
                            <h1 className="text-[14px] text-gray-500">
                              Employee Number : 233
                            </h1>
                          </div>
                          <img
                            src={logo}
                            className="h-10 object-contain max-w-full"
                            alt=""
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="flip-card-back">
                  <div className="h-[550px] w-[380px] relative rounded-2xl overflow-hidden shadow-2xl">
                    <img src={cardBg} className="object-cover h-full w-full" alt="" />
                    <div className="absolute top-0 bottom-0 left-0 right-0">
                      <div className=" h-full w-full mt-4 px-4 pb-7">
                        <div className="flex flex-col h-full">
                          <h1 className="text-[24px] my-4 text-gray-800 font-semibold">
                            Additional Information
                          </h1>
                          <div className="grow">
                            <div className="flex gap-5">
                              <p className="text-[16px] my-1 text-gray-700 font-semibold">
                                Blood Type:
                              </p>
                              <p className="text-[16px] my-1 text-purple-700 font-bold">
                                A+
                              </p>
                            </div>
                            <div className="flex gap-5">
                              <p className="text-[16px] my-1 text-gray-700 font-semibold">
                                Emergency Contact:
                              </p>
                              <p className="text-[16px] my-1 text-purple-700 font-bold">
                                98546258778
                              </p>
                            </div>
                            <div className="flex gap-5">
                              <p className="text-[16px] my-1 text-gray-700 font-semibold">
                                Issued Date:
                              </p>
                              <p className="text-[16px] my-1 text-purple-700 font-bold">
                                2078-04-05
                              </p>
                            </div>
                          </div>
                          <img
                            src={logo}
                            className="h-10 object-contain max-w-full"
                            alt=""
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card shadow-2xl">
              <div className="mb-4">
                <label
                  htmlFor="username"
                  className="block text-lg font-medium leading-6 text-gray-900"
                >
                  Name
                </label>
                <div className="mt-2">
                  <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                    <input
                      type="text"
                      name="Name"
                      id="Name"
                      autoComplete="Name"
                      className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 pl-3"
                      placeholder="Hari Sharan Shrestha"
                    />
                  </div>
                </div>
              </div>

              <div className="mb-4">
                <label
                  htmlFor="username"
                  className="block text-lg font-medium leading-6 text-gray-900"
                >
                  Designation
                </label>
                <div className="mt-2">
                  <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                    <input
                      type="text"
                      name="Name"
                      id="Name"
                      autoComplete="Name"
                      className="block flex-1 border-0 bg-transparent py-1.5 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 pl-3"
                      placeholder="Senior Frontend Developer"
                    />
                  </div>
                </div>
              </div>
              <div className="mb-4">
                <label
                  htmlFor="username"
                  className="block text-lg font-medium leading-6 text-gray-900"
                >
                  Employee Number
                </label>
                <div className="mt-2">
                  <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                    <span className="flex select-none items-center pl-3 text-gray-500 sm:text-sm">
                      Employee Number:
                    </span>
                    <input
                      type="text"
                      name="Name"
                      id="Name"
                      autoComplete="Name"
                      className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                      placeholder="223"
                    />
                  </div>
                </div>
              </div>
              <button className="bg-white text-purple-800 border-purple-400 py-2 rounded-md">
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default idCard;
