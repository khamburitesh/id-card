import { Suspense, lazy } from "react";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import "./App.css";
import ErrorPage from "./ErrorPage";
import IdCard from "./IdCard";

// import './index.css'


const Form = lazy(() => import("./Form"));

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <Suspense>
        <IdCard />
      </Suspense>
    ),
    errorElement: <ErrorPage />,
  },
  {
    path: "/form",
    element: (
      <Suspense>
        <Form />
      </Suspense>
    ),
    errorElement: <ErrorPage />,
  },
])

function App() {
  return (
    <>
    
    <RouterProvider router={router} />
    </>
  );
}



export default App;
